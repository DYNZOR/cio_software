package experiments;

import interfaces.Experiment;
import interfaces.Algorithm;
import interfaces.Problem;

import algorithms.SimulatedAnnealing; 

import benchmarks.BaseFunctions.DeJong; 
import benchmarks.BaseFunctions.Schwefel;
import benchmarks.BaseFunctions.Rastigin;
import benchmarks.BaseFunctions.Michalewicz;

public class SAnnealingTest extends Experiment
{
	
	public SAnnealingTest(int probDim)
	{
		super(probDim,"SAnnealing_Test");
			
		Algorithm a;// ///< A generic optimiser.
	    Problem p;// ///< A generic problem.

		a = new SimulatedAnnealing();
		add(a); //add it to the list


		p = new DeJong(probDim);
		add(p);//add it to the list
		
		p = new Schwefel(probDim);
		add(p);
		
		p = new Rastigin(probDim);
		add(p);

		p = new Michalewicz(probDim);
		add(p);



	}
}
