package experiments;

import interfaces.Experiment;
import interfaces.Algorithm;
import interfaces.Problem;
import algorithms.OnePlsOne_ES_V1; 
import algorithms.OnePlsOne_ES_V2;
import benchmarks.BaseFunctions.Michalewicz;
import benchmarks.BaseFunctions.Rastigin;
import benchmarks.BaseFunctions.Schwefel;
import benchmarks.BaseFunctions.DeJong;

public class OnePlsOneVariants extends Experiment
{
	
	public OnePlsOneVariants(int probDim)
	{
		super(probDim,"Task6_1+1ES_Variants");
			
		Algorithm a;// ///< A generic optimiser.
	    Problem p;// ///< A generic problem.

	    a = new OnePlsOne_ES_V1();
	    add(a);
	    a = new OnePlsOne_ES_V2();
	    add(a);
		
		p = new DeJong(probDim);
		add(p);//add it to the list
		p = new Schwefel(probDim);
		add(p);
		
		p = new Rastigin(probDim);
		add(p);
		p = new Michalewicz(probDim);
		add(p);
	}
}
