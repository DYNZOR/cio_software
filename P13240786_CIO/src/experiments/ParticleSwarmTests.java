package experiments;

import interfaces.Experiment;
import interfaces.Algorithm;
import interfaces.Problem;
import algorithms.PSO; 
import algorithms.CLPSO;
import algorithms.OnePlsOne_ES_V2;
import benchmarks.BaseFunctions.Michalewicz;
import benchmarks.BaseFunctions.Rastigin;
import benchmarks.BaseFunctions.Schwefel;
import benchmarks.BaseFunctions.DeJong;

public class ParticleSwarmTests extends Experiment
{
	
	public ParticleSwarmTests(int probDim)
	{
		super(probDim,"Task7_PSO&CLPSO");
			
		Algorithm a;// ///< A generic optimiser.
	    Problem p;// ///< A generic problem.

	    //a = new PSO();
	    //add(a);
	    a = new CLPSO();
	    add(a);
		
		//p = new DeJong(probDim);
		//add(p);
		//p = new Schwefel(probDim);
		//add(p);
		p = new Rastigin(probDim);
		add(p);
		p = new Michalewicz(probDim);
		add(p);
	}
}
