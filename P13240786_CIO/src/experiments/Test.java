package experiments;

import interfaces.Experiment;
import interfaces.Algorithm;
import interfaces.Problem;
import algorithms.*; 
import algorithms.ga.GA;
import benchmarks.BaseFunctions.Alpine; 
import benchmarks.BaseFunctions.Ackley;
import benchmarks.BaseFunctions.Rosenbrock;
import benchmarks.BaseFunctions.Michalewicz;
import benchmarks.BaseFunctions.Rastigin;
import benchmarks.BaseFunctions.Schwefel;
import benchmarks.BaseFunctions.DeJong;

public class Test extends Experiment
{
	
	public Test(int probDim)
	{
		super(probDim,"Task5_GA_Algorithm");
			
		Algorithm a;// ///< A generic optimiser.
	    Problem p;// ///< A generic problem.

//		a = new ISPO();
//		a.setParameter("p0", 1.0);
//		a.setParameter("p1", 10.0);
//		a.setParameter("p2", 2.0);
//		a.setParameter("p3", 4.0);
//		a.setParameter("p4", 1e-5);
//		a.setParameter("p5", 30.0);
//		add(a); //add it to the list

		//a = new CMAES(); //N.B. this algorithm makes use of "generateRandomSolution" that has still to be implemented.
		//add(a);
		//a = new S();
		//add(a);
		//a = new SimulatedAnnealing();
		//add(a);
		a = new GA();
		add(a);

		//a = new RIS();
		//add(a);
	    
		p = new DeJong(probDim);
		add(p);//add it to the list
		p = new Schwefel(probDim);
		add(p);
		
		p = new Rastigin(probDim);
		add(p);
		p = new Michalewicz(probDim);
		add(p);

	}
}
