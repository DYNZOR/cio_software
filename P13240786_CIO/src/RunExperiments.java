/** @file RunExperiments.java
 *  
 *  @author Fabio Caraffini
*/
import java.util.Vector; 
import interfaces.Experiment;
import utils.random.RandUtils;

import static utils.RunAndStore.resultsFolder;
import experiments.*;

/** 
* This class contains the main method and has to be used for launching experiments.
*/
public class RunExperiments
{
	
	/** 
	* Main method.
	* This method has to be modified in order to launch a new experiment.
	*/
	public static void main(String[] args) throws Exception
	{	
		
		// make sure that "results" folder exists
		resultsFolder();
	
	
		Vector<Experiment> experiments = new Vector<Experiment>();////!< List of problems 
	
		
		experiments.add(new Test(10));
		//experiments.add(new Test(30));
		experiments.add(new Test(50));
		//experiments.add(new Test(100));

		//experiments.add(new SAlgorithmTest(10));
		
		//experiments.add(new SAnnealingTest(10));
		//experiments.add(new SAnnealingTest(30));
		//experiments.add(new SAnnealingTest(50));

	
		// 1+1ES Variants Tests 
		//experiments.add(new OnePlsOneVariants(10));
		//experiments.add(new OnePlsOneVariants(50));
		
		// PSO & CLPSO Tests 
		//experiments.add(new ParticleSwarmTests(10));
		//experiments.add(new ParticleSwarmTests(50));
	
		for(Experiment experiment : experiments)
		{
			//experiment.setShowPValue(true);
	
			experiment.setNrRuns(30);
			//experiment.setBudgetFactor(arg0);
			experiment.startExperiment();
			System.out.println();
			experiment = null;
		}
	
		
		
	}
	
	

}
