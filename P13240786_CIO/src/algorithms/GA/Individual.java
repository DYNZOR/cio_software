package algorithms.ga;

import static utils.algorithms.Misc.generateRandomSolution;

import java.util.Random;
import interfaces.Problem;

public class Individual {
	
	static int SIZE = 0; // correct this
	
	public double[] genes; //= new double[SIZE];
	private double fitnessValue; 
	
	Problem p;
	
	public Individual(Problem pIn) {
		p = pIn; 
		SIZE = p.getDimension();
		this.genes = new double[SIZE];
	}
	
	public double getFitnessValue()
	{
		return fitnessValue;
	}
	
	public void setFitnessValue(double fitnessValIn)
	{
		this.fitnessValue = fitnessValIn;
	}
	
	public double getGene(int index)
	{
		return genes[index];
	}
	
	public void setGene(int index, double gene)
	{
		this.genes[index] = gene;
	}
	
	public void setGenes(double[] genes)
	{
		this.genes = genes.clone();
	}
	
	public void randGenes()
	{
		double[] RandomGenes = generateRandomSolution(p.getBounds(), p.getDimension());
		this.setGenes(RandomGenes);
	}
	
	public void mutate() 
	{
		Random rand = new Random();
		int index = rand.nextInt(genes.length);
		this.setGene(index, 1-this.getGene(index));
	}
	
	public double evaluate() throws Exception
	{
		double fitness = p.f(genes);
		
        this.setFitnessValue(fitness);
        
        return fitness;
	}
	
	public int GetGeneLength()
	{
		return genes.length;
	}
	
}

