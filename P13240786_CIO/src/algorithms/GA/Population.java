package algorithms.ga;

import java.util.Random;
import interfaces.Problem;

public class Population {

	public int PopulationSize = 0;
    private double totalFitness;

    private static Random m_rand = new Random();  // random-number generator
    private Individual[] m_population;
    
    public static Problem pProb;

    public Population(Problem pIn, int popSize) throws Exception {
        PopulationSize = popSize;
    	m_population = new Individual[PopulationSize];
        pProb = pIn;
        
        // init population
        for (int i = 0; i < PopulationSize; i++) {
            m_population[i] = new Individual(pProb);    
            m_population[i].randGenes();
        }

        // evaluate current population
        this.evaluate();
    }

    public void setPopulation(Individual[] newPop) {
        // this.m_population = newPop;
        System.arraycopy(newPop, 0, this.m_population, 0, PopulationSize);
    }

    public Individual[] getPopulation() {
        return this.m_population;
    }

    public double evaluate() throws Exception {
        this.setTotalFitness(0.0);
        for (int i = 0; i < PopulationSize; i++) {
        	this.totalFitness += m_population[i].evaluate();
        }
        this.setTotalFitness(totalFitness);
        
        return this.getTotalFitness();
    }

    public Individual tournamentSelection() throws Exception {
    	    	
    	int randIndiv1Index = 0;
    	int randIndiv2Index = 0;
    	
    	// Ensure that indexes are different 
    	while(randIndiv1Index == randIndiv2Index)
    	{
    		randIndiv1Index = m_rand.nextInt(m_population.length -1);
    	    randIndiv2Index = m_rand.nextInt(m_population.length -1);
    	}
    	
    	Individual IndivWinner = new Individual(pProb);
    	

    	Individual indiv1 = m_population[randIndiv1Index];
    	indiv1.evaluate();
    	double indiv1fitness = indiv1.getFitnessValue();
    		
    	Individual indiv2 = m_population[randIndiv2Index];
    	indiv2.evaluate();
    	double indiv2fitness = indiv2.getFitnessValue();
    		
    	if (indiv1fitness < indiv2fitness)
    	{
    		IndivWinner = indiv1;
    	}
    	else if (indiv2fitness < indiv1fitness )
    	{
    		IndivWinner = indiv2;
    	}
    		
    	return IndivWinner;
    }

    public Individual findBestIndividual() {
        int idxMax = 0, idxMin = 0;
        double currentMax = 0.0;
        double currentMin = 1.0;
        double currentVal;

        for (int idx=0; idx<PopulationSize; ++idx) {
            currentVal = m_population[idx].getFitnessValue();
            if (currentMax < currentMin) {
                currentMax = currentMin = currentVal;
                idxMax = idxMin = idx;
            }
            if (currentVal > currentMax) {
                currentMax = currentVal;
                idxMax = idx;
            }
            if (currentVal < currentMin) {
                currentMin = currentVal;
                idxMin = idx;
            }
        }

        //return m_population[idxMin];      // minimization
        return m_population[idxMax];        // maximization
    }

    public static Individual[] randomCrossover(Individual indiv1, Individual indiv2) {
        Individual[] newIndiv = new Individual[2];
        newIndiv[0] = new Individual(pProb);
        newIndiv[1] = new Individual(pProb);

        int randPoint = m_rand.nextInt(Individual.SIZE);
        int i;
        for (i=0; i<randPoint; ++i) {
            newIndiv[0].setGene(i, indiv1.getGene(i));
            newIndiv[1].setGene(i, indiv2.getGene(i));
        }
        for (i=0; i< Individual.SIZE; ++i) {
            newIndiv[0].setGene(i, indiv2.getGene(i));
            newIndiv[1].setGene(i, indiv1.getGene(i));
        }

        return newIndiv;
    }
    
    public static Individual[] boxCrossover(Individual indiv1, Individual indiv2) {
    	
        Individual[] newIndiv = new Individual[2];
        newIndiv[0] = new Individual(pProb);
        newIndiv[1] = new Individual(pProb);
        
        for (int i = 0; i < indiv1.genes.length; i++)
        {
        	double a = m_rand.nextDouble();
        	double[] ix = indiv1.genes;
        	double[] iy = indiv2.genes;
        	
        	newIndiv[0].setGene(i, (Math.min(ix[i], iy[i]) + (a*Math.abs(ix[i]-iy[i]))));
        	a = m_rand.nextDouble();
        	
        	newIndiv[1].setGene(i, (Math.min(ix[i], iy[i]) + (a*Math.abs(ix[i]-iy[i]))));

        }

        return newIndiv;
    }

	public double getTotalFitness() {
		return totalFitness;
	}

	public void setTotalFitness(double totalFitness) {
		this.totalFitness = totalFitness;
	}
}
