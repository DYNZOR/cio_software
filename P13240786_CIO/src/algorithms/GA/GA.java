package algorithms.ga;

import static utils.algorithms.Misc.generateRandomSolution;

import java.util.Random;

import com.sun.org.apache.bcel.internal.generic.POP;

import interfaces.Algorithm;

import interfaces.Problem;
import utils.RunAndStore.FTrend;

public class GA extends Algorithm {
	
	@Override
	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		
		double fBest = Double.NaN;
				
		int j = 0;
		
		Random rand = new Random();
		int populationSize = 20;
		double mutationRate = 0.1;
		double crossoverRate = 0.7;
		
		Population pop = new Population(problem, populationSize);
		Individual[] newPop = new Individual[populationSize];
		Individual[] indiv = new Individual[2];

        int count;
     
        for (int itr = 0; itr < maxEvaluations; ++itr)
        {
        	count = 0;
        	
        	while (count < populationSize)
        	{
        		indiv[0] = pop.tournamentSelection();
        		indiv[1] = pop.tournamentSelection();
        		
        		// Cross over
        		if (rand.nextDouble() < crossoverRate)
        		{
        			indiv = Population.boxCrossover(indiv[0], indiv[1]);
        		}
        		
        		// Mutation 
        		if (rand.nextDouble() < mutationRate)
        		{
        			indiv[0].mutate();
        		}
        		
        		// Mutation 
        		if (rand.nextDouble() < mutationRate)
        		{
        			indiv[1].mutate();
        		}
        		
        		newPop[count] = indiv[0];
        		newPop[count+1] = indiv[1];
        		count+=2;	
        	}
        	// Update population
            pop.setPopulation(newPop);
            
            // Evaluate population
            pop.evaluate();
            j++;
        }
        
        // best indiv
        Individual bestIndiv = pop.findBestIndividual();
        
        finalBest = bestIndiv.genes;
		fBest = problem.f(bestIndiv.genes);
		
		FT.add(j, fBest);//add it to the txt file (row data)

		return FT; //return the fitness trend
	}   
}
