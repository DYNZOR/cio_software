package algorithms;
import static utils.algorithms.Misc.generateRandomSolution;
import static utils.algorithms.Misc.toro;

import java.util.Random;

import interfaces.Algorithm;
import interfaces.Problem;
import utils.random.RandUtils;
import utils.MatLab;
import utils.RunAndStore.FTrend;
/**
 * RIS Memetic Algorithm 
 */
public class RIS extends Algorithm //This class implements the algorithm. Every algorithm will have to contain its specific implementation within the method "execute". The latter will contain a main loop performing the iterations, and will have to return the fitness trend (including the final best) solution. Look at this examples before implementing your first algorithm.
{
	@Override

	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] initialBest = new double[problemDimension];
		double fBest = Double.NaN; //fitness value, i.e. "f(x)"
		
		int j = 0;

		// initial solution
		if (initialSolution != null)
		{
			initialBest = initialSolution.clone();
			//xBest = initialSolution;  
		    fBest = initialFitness;
		}
		else//random initial guess
		{
			initialBest = generateRandomSolution(bounds, problemDimension).clone();
			//xBest = generateRandomSolution(bounds, problemDimension);
			fBest = problem.f(initialBest);
			FT.add(j, fBest);
			j++;
		}
		
		// Elite solution i.e global best 
		double[] xE = initialBest.clone();
		
		// Initial trial solution 
		double[] xT = initialBest.clone();
		double[] xS = new double[problemDimension];
		
		// Random gene to sample 
		double random = RandUtils.uniform(0, 0.9);
		int index = (int) Math.round(problemDimension * random );
		
		// Crossover rate 
		double Cr = 1/0.5*Math.sqrt(2);
		
		xT[index] = xE[index];
		
		// P exploratory radius vector 
		double alpha = 0.4;
		double[] P = new double[bounds.length];
		int ds = P.length;
				
		for (int i= 0; i < ds; i++){
			P[i] = alpha*(bounds[i][1] - bounds[i][0]);
		}
				
		
		// RIS //
		int itr = 0;
		while (itr < maxEvaluations)
		{
			// Re-Sampling Inheritance Search 
			int k = 1;
			while (RandUtils.uniform(0, 1) <= Cr && k < problemDimension)
			{
				xT[index] = xE[index];
				index = index + 1;
				
				if (index == problemDimension)
				{
					index = 1;
				}
				k = k + 1;
			}
			
			if (problem.f(xT) <= problem.f(xE))
			{
				xE = xT.clone();
			}
			
			
			// Exploitative Local Search 
			while (j < maxEvaluations)
			{
				xS = xT.clone();
				
				for (int i = 0; i < problemDimension; ++i)
				{
					xS[i] = xT[i] - P[i];
					
					if (problem.f(xS) <= problem.f(xT))
					{
						xT = xS.clone();
					} 
					else 
					{
						xS[i] = xT[i] + P[i]/2;
						if (problem.f(xS) <= problem.f(xT))
						{
							xT = xS.clone();
						}
					}
				}
				if (problem.f(xT) <= problem.f(xE))
				{
					xE = xT.clone();
				} 
				else 
				{
					for (int i = 0; i < problemDimension; ++i)
					{
						P[i] = P[i] /2;
					}
				}
				j++; 
			}
			itr++;
		}
		
		finalBest = xE.clone(); //save the final best
		fBest = problem.f(finalBest);
		FT.add(j, fBest);//add it to the txt file (row data)
		//FT.add(j, problem.f(finalBest));//add it to the txt file (row data)

		return FT; //return the fitness trend
	}
}
