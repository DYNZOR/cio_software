package algorithms;

import static utils.algorithms.Misc.generateRandomSolution;

import interfaces.Algorithm;
import interfaces.Problem;
import utils.RunAndStore.FTrend;

public class OnePlsOne_ES_V1 extends Algorithm {
	
	@Override
	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] initialBest = new double[problemDimension];
		double fBest = Double.NaN; //fitness value, i.e. "f(x)"
		
		int j = 0;

		// initial solution
		if (initialSolution != null)
		{
			initialBest = initialSolution.clone();
		    fBest = initialFitness;
		}
		else//random initial guess
		{
			initialBest = generateRandomSolution(bounds, problemDimension).clone();
			fBest = problem.f(initialBest);
			FT.add(j, fBest);
			j++;
		}
		
		double[] X = initialBest.clone();
		double[] Xoffspring = new double[problemDimension];
		double[] stepSizes = new double[problemDimension];
		
		for(int i = 0; i < stepSizes.length; ++i)
		{
			stepSizes[i] = 1/5;
		}
		
		while (j < maxEvaluations)
		{
			for (int i = 0; i < X.length; ++i)
			{
				Xoffspring[i] = X[i] + stepSizes[i];/* * N(0,I); */ 
				
				//if (Xoffspring[i] < X[i]) // CHECK 
				if (problem.f(Xoffspring) < problem.f(X)) // CHECK 

				{
					X[i+1] = Xoffspring[i]; // New parent = offspring 
					stepSizes[i+1] = 1.5*stepSizes[i]; // Increase step size 
				}
				//else if(Xoffspring[i] == X[i])
				else if (problem.f(Xoffspring) == problem.f(X)) // CHECK 
				{
					if (i+1 >= 0 && i+1 < X.length)
					{
						X[i+1] = X[i];
					} 
					else
					{
						X[i] = X[i];
					}
					
					if (i+1 >= 0 && i+1 < X.length)
					{
						stepSizes[i+1] = stepSizes[i];
					} 
					else // CHECK THIS!!
					{
						stepSizes[i] = stepSizes[i];
					}
				}
				else // If offspring is worse 
				{
					X[i+1] = X[i]; // New parent = old parent 
					stepSizes[i+1] = Math.pow(1.5, -1/4)*stepSizes[i]; // Decrease step size 

				}
			}
			j++;
		}
		

		finalBest = X.clone(); //save the final best
		
		FT.add(j, problem.f(finalBest));//add it to the txt file (row data)

		return FT; //return the fitness trend
	}
}
