//package algorithms;
////in this part you can import the functionalities that yuo need to use for implementing your algorithm
//import static utils.algorithms.Misc.generateRandomSolution;
//import java.util.Random;
//
//import algorithms.ga.Individual;
//import algorithms.ga.Population;
//import interfaces.Algorithm;
//import interfaces.Problem;
//import utils.RunAndStore.FTrend;
//import utils.RunAndStore.AlgorithmResult;
///*
// * DE Algorithm 
// */
//public class DE_Rand_1_Exp extends Algorithm //This class implements the algorithm. Every algorithm will have to contain its specific implementation within the method "execute". The latter will contain a main loop performing the iterations, and will have to return the fitness trend (including the final best) solution. Look at this examples before implementing your first algorithm.
//{
//	@Override
//
//	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
//	{
//		FTrend FT = new FTrend();
//		int problemDimension = problem.getDimension(); 
//		double[][] bounds = problem.getBounds();
//		
//		Population pop = new Population(problem);
//		
//		Individual[] newPop = new Individual[10];
//		Individual[] indiv = new Individual[2];
//		
//		double[] POPg = new double[problemDimension];		
//		double xBest = Double.NaN; //fitness value, i.e. "f(x)"
//		
//		int [] Xj = new int[problemDimension];
//		
//		int dimension = 0;
//		for (int i = 0; i < Xj.length; i++)
//		{
//			Xj[i] = dimension;
//			dimension++;
//		}
//		
//		int row = 0;
//		// initial solution
//		if (initialSolution != null)
//		{
//			
//			POPg = initialSolution;
//		    xBest = initialFitness;
//		}
//		else//random initial guess
//		{
//			POPg = generateRandomSolution(bounds, problemDimension);
//			xBest = problem.f(POPg);
//			row++;
//		}
//		
//		//store the initial guess
//		FT.add(0, xBest);
//		
//		//main loop
//		while (row < maxEvaluations)
//		{
//			//for (Xj = 0; Xj < POPg.length; Xj++)
//			//{
//				
//			//}
//		}
//		
//		
//		//finalBest = xCb; //save the final best
//		
//		FT.add(row, problem.f(finalBest));//add it to the txt file (row data)
//
//		
//		return FT; //return the fitness trend
//	}
//}
