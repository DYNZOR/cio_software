package algorithms;
//in this part you can import the functionalities that you need to use for implementing your algorithm
import static utils.algorithms.Misc.generateRandomSolution;
import static utils.algorithms.Misc.toro;
import java.util.Random;
import java.util.Vector;
import interfaces.Algorithm;
import interfaces.Problem;
import jdk.management.resource.internal.TotalResourceContext;
import utils.RunAndStore.FTrend;
import utils.random.RandUtils;
import utils.RunAndStore.AlgorithmResult;
/*
 * Simulated Annealing Algorithm 
 */
public class SimulatedAnnealing extends Algorithm //This class implements the algorithm. Every algorithm will have to contain its specific implementation within the method "execute". The latter will contain a main loop performing the iterations, and will have to return the fitness trend (including the final best) solution. Look at this examples before implementing your first algorithm.
{
	@Override

	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();
		
		// particle (the solution, i.e. "x")
		double[] xCb = new double[problemDimension];		
		
		double T = 1; 
		double alpha = 0.5;
		
		int itr = 0;
		// initial solution
		if (initialSolution != null)
		{
			
			xCb = initialSolution.clone();
		}
		else//random initial guess
		{
			xCb = generateRandomSolution(bounds, problemDimension);
			itr++;
		}
		
		//store the initial guess
		FT.add(0, problem.f(xCb.clone()));
		
		//main loop
		while (itr < maxEvaluations)
		//while (row < fComputeBudget)
		{
			//double[] xNew = PerturbationSolution(problem, xCb);
			double[] xNew = RandomPointSolution(problem, xCb);

			xNew = toro(xNew, bounds).clone();
	
			if ( RandUtils.uniform(0,1) < (Math.E*(problem.f(xCb) - problem.f(xNew))) / T )
			{
				xCb = xNew.clone(); 
			}
			
			// Reduce T 
			T *= alpha; // or 0.8 
			//T /= itr; // or 0.8 

			itr++;
		}
		
		
		finalBest = xCb; //save the final best
		
		FT.add(itr, problem.f(finalBest));//add it to the txt file (row data)

		
		return FT; //return the fitness trend
	}
	
	double [] PerturbationSolution(Problem p, double[] initialSolutionIn) throws Exception
	{
		int dimension = p.getDimension();
		double[][] bounds = p.getBounds();
		
		double[] initialSolution = initialSolutionIn.clone();
		double[] xNew = new double[initialSolution.length];
		
		// Random solution to perturb
		double[] xRandom = generateRandomSolution(bounds, dimension);

		for (int i = 0; i < initialSolution.length; ++i) {
				
		    xNew[i] = initialSolution[i] + xRandom[i];
		}
			
		if (p.f(xNew.clone()) < p.f(initialSolution.clone()))
		{
			initialSolution = xNew.clone();
		}
		return initialSolution;
	}
	
	double [] RandomPointSolution(Problem p, double[] initialSolutionIn) throws Exception
	{
		int dimension = p.getDimension();
		double[][] bounds = p.getBounds();
		
		double[] initialSolution = initialSolutionIn.clone();
		double[] xNew = new double[initialSolution.length];
		
		double[] xRandom = generateRandomSolution(bounds, dimension);
		xNew = xRandom.clone();
			
		if (p.f(xNew.clone()) < p.f(initialSolution.clone()))
		{
			initialSolution = xNew.clone();
		}
		return initialSolution;
	}
	
	
}



