package algorithms;
//in this part you can import the functionalities that yuo need to use for implementing your algorithm
import static utils.algorithms.Misc.generateRandomSolution;
import static utils.algorithms.Misc.toro;
import static utils.MatLab.max;
import static utils.MatLab.min;

import java.util.Vector;
import interfaces.Algorithm;
import interfaces.Problem;
import utils.random.RandUtils;
import utils.RunAndStore.FTrend;
/**
 * S Algorithm 
 */
public class S extends Algorithm //This class implements the algorithm. Every algorithm will have to contain its specific implementation within the method "execute". The latter will contain a main loop performing the iterations, and will have to return the fitness trend (including the final best) solution. Look at this examples before implementing your first algorithm.
{
	@Override

	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] initialBest = new double[problemDimension];
		double fBest = Double.NaN; //fitness value, i.e. "f(x)"
		
		int j = 0;

		// initial solution
		if (initialSolution != null)
		{
			initialBest = initialSolution.clone();
			//xBest = initialSolution;  
		    fBest = initialFitness;
		}
		else//random initial guess
		{
			initialBest = generateRandomSolution(bounds, problemDimension).clone();
			//xBest = generateRandomSolution(bounds, problemDimension);
			fBest = problem.f(initialBest);
			FT.add(j, fBest);
			j++;
		}
		
		double alpha = 0.4;
		
		double[] delta = new double[bounds.length];
		int ds = delta.length;
		
		for (int i= 0; i < ds; i++){
			delta[i] = alpha*(bounds[i][1] - bounds[i][0]);
		}
		
		double[] xBest = initialBest.clone();
		double[] xS = initialBest.clone();
		
		//main loop
		while (j < maxEvaluations)
		{
			for (int i = 0; i < xBest.length; i++)
			{
				xS[i] = xBest[i] - delta[i];
				
				if (problem.f(xS.clone()) <= problem.f(xBest.clone()))
				{
					xBest[i] = xS[i];
				}
				else 
				{
					xS[i] = xBest[i];
					xS[i] = xBest[i] + ( delta[i] / 2 );
					
					if (problem.f(xS.clone()) <= problem.f(xBest.clone()))
					{
						xBest[i] = xS[i];
					}
					else 
					{
						xS[i] = xBest[i];
					} 
				}
			}
			if (problem.f(toro(xBest.clone(),bounds)) <= problem.f(toro(initialBest.clone(), bounds)))
			{
				for (int i = 0; i < delta.length; i++)
				{
					delta[i] = delta[i]/2;
				}
			} 
			j++; 
		}
		
		
		finalBest = xBest.clone(); //save the final best
		
		FT.add(j, problem.f(toro(finalBest.clone(), bounds)));//add it to the txt file (row data)
		//FT.add(j, problem.f(finalBest));//add it to the txt file (row data)

		return FT; //return the fitness trend
	}
}
