package algorithms;
//in this part you can import the functionalities that yuo need to use for implementing your algorithm
import static utils.algorithms.Misc.generateRandomSolution;
import static utils.algorithms.Misc.toro;

import interfaces.Algorithm;
import interfaces.Problem;
import utils.RunAndStore.FTrend;
import utils.random.RandUtils;
import utils.MatLab;
/**
 * CLPSO Algorithm 
 */
public class CLPSO extends Algorithm //This class implements the algorithm. Every algorithm will have to contain its specific implementation within the method "execute". The latter will contain a main loop performing the iterations, and will have to return the fitness trend (including the final best) solution. Look at this examples before implementing your first algorithm.
{
	@Override
	public FTrend execute(Problem problem, int maxEvaluations) throws Exception
	{
		FTrend FT = new FTrend();
		int problemDimension = problem.getDimension(); 
		double[][] bounds = problem.getBounds();

		double[] initialBest = new double[problemDimension];
		double fBest = Double.NaN; //fitness value, i.e. "f(x)"
		
		int j = 0;

		// initial solution
		if (initialSolution != null)
		{
			initialBest = initialSolution.clone();
		    fBest = initialFitness;
		}
		else//random initial guess
		{
			initialBest = generateRandomSolution(bounds, problemDimension).clone();
			fBest = problem.f(initialBest);
			FT.add(j, fBest);
			j++;
		}
	
		// Swarm size 
		//int NP = 10;
		int NP = problemDimension;
		double[] vMax = new double[bounds.length];
		double[] vMin = new double[bounds.length];
		int ds = vMax.length;
		
		for (int i= 0; i < ds; i++){
			vMax[i] = (bounds[i][1] - bounds[i][0]);
			vMin[i] = -vMax[i];
		}
		
		double[] xi = initialBest.clone();
	    double[] vi = new double[NP];
	    double[] xLb = new double[NP]; // Local best positions 
	    double[] xGb = new double[NP];
	    double[] xRlb = new double [NP];
	    double inertiaWeight = 0.7;
	    double constWeight = 0.5;
	    
		// Declare matrix U
		double[][] U = new double[problemDimension][problemDimension];
		
		// Initialise U 
		for (int i = 0; i < problemDimension; ++i)
		{
			for (int k = 0; k < problemDimension; ++k)
			{
				U[i][k] = RandUtils.uniform(0, 1);
			}
		}
		
	    
		for (int i = 0; i < NP; i++)
		{
			vi[i] = RandUtils.uniform(vMin[i]/3, vMax[i]/3);
			xi[i] = RandUtils.uniform(vMin[i]/3, vMax[i]/3); // (xL, xU)
			
			xLb[i]= xi[i];
			
			if (problem.f(xi) <= problem.f(xGb) || i == 1)
			{
				xGb[i] = xi[i];			
			}
		}
		
		while (j < maxEvaluations)
		{
			for (int i = 0; i < NP; ++i)
			{
				if (RandUtils.uniform(0,1) > xi[i] )
				{
					xRlb[i] = xLb[i];
				}
				// Update velocities and perturb positions
				for (int matJ = 0; matJ < problemDimension; ++matJ)
				{
					vi[i] = inertiaWeight*vi[i] + constWeight * U[i][matJ] * (xRlb[i] - xi[i]);
					
					xi[i] = xi[i] + vi[i];
				}
			
				if (problem.f(xi) <= problem.f(xLb))
				{
					xLb[i] = xi[i];
					
					if (problem.f(xi) <= problem.f(xGb))
					{
						xGb[i] = xi[i];
					}
					
				}
				
			}
			
			j++;
		}
		
		finalBest = xGb.clone(); //save the final best
		
		FT.add(j, problem.f(finalBest));//add it to the txt file (row data)

		return FT; //return the fitness trend
	}
}
